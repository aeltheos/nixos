{ pkgs, ... }:
pkgs.mkShell {
  buildInputs = [
    pkgs.nixd
    pkgs.agenix-rekey
  ];
}
