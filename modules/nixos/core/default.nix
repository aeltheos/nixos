{
  config,
  flake,
  pkgs,
  ...
}:
{
  networking.useDHCP = false;
  networking.useNetworkd = true;

  nix = {
    optimise.automatic = true;
    gc.automatic = true;
    extraOptions = ''
      experimental-features = flakes nix-command
    '';
  };

  i18n.defaultLocale = "en_US.UTF-8";
  console.keyMap = "fr";
  time.timeZone = "Europe/Paris";

  programs.git.enable = true;
  programs.tmux.enable = true;
  environment.systemPackages = [ pkgs.helix ];

  users.mutableUsers = false;

  services.openssh = {
    enable = true;

    authorizedKeysInHomedir = false;
    settings.PasswordAuthentication = false;
  };

  users.users.root = {
    hashedPasswordFile = config.age.secrets.rootHashedPassword.path;
    openssh.authorizedKeys.keyFiles = [
      (flake + /keys/users/aeltheos/ssh-yubikey-19666063.pub)
      (flake + /keys/users/aeltheos/ssh-yubikey-19666099.pub)
    ];
  };
}
