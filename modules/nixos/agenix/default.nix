{
  flake,
  lib,
  config,
  inputs,
  ...
}:
{
  imports = [
    inputs.agenix.nixosModules.default
    inputs.agenix-rekey.nixosModules.default
  ];

  age.rekey = {
    hostPubkey =
      let
        path = flake + /keys/hosts/murozond/ssh_host_ed25519_key.pub;
      in
      lib.mkIf (builtins.pathExists path) (builtins.readFile path);

    masterIdentities = [ (flake + /keys/users/aeltheos/age-identity-yubikey-19666063.pub) ];

    storageMode = "local";

    localStorageDir = flake + /secrets/rekeyed/${config.networking.hostName};
  };

  age.generators.long-passphrase = { pkgs, ... }: "${lib.getExe pkgs.xkcdpass} --numwords=10";

  age.generators.mkpasswd =
    {
      decrypt,
      pkgs,
      deps,
      ...
    }:
    let
      dep = builtins.head deps;
    in
    ''
      ${decrypt} ${lib.escapeShellArg dep.file} \
      | ${pkgs.mkpasswd}/bin/mkpasswd \
        --method=scrypt \
        --stdin \
      || die "failed to compute scrypt hash"
    '';

  age.secrets.rootPassword = {
    rekeyFile = flake + /secrets/master/hosts/${config.networking.hostName}/rootPassword.age;
    generator.script = "long-passphrase";
    intermediary = true;
  };

  age.secrets.rootHashedPassword = {
    rekeyFile = flake + /secrets/master/hosts/${config.networking.hostName}/rootHashedPassword.age;
    generator.dependencies = [ config.age.secrets.rootPassword ];
    generator.script = "mkpasswd";
  };
}
