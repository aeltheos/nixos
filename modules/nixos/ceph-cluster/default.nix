{pkgs,  ... }:
{
  services.ceph = {
    enable = true;

    global = {
      fsid = "88f7699f-cc56-45fe-8a8e-92cd0a6d376c";
      monHost = "185.230.78.42";
      monInitialMembers = "murozond";
    };

    client = {
      enable = true;
    };
  };

  environment.systemPackages = [ pkgs.ceph ];

  systemd.services.ceph-mount = {
    enable = true;
    description = "Ceph OSD Bindings";
    unitConfig = {
      After = "local-fs.target";
      Wants = "local-fs.target";
    };
    serviceConfig = {
      Type = "oneshot";
      KillMode = "none";
      Environment = "CEPH_VOLUME_TIMEOUT=10000 PATH=$PATH:/run/current-system/sw/bin/";
      ExecStart = "/bin/sh -c 'timeout $CEPH_VOLUME_TIMEOUT /run/current-system/sw/bin/ceph-volume lvm activate --all --no-systemd'";
      TimeoutSec = 0;
    };
    wantedBy = [ "multi-user.target" ];
  };
}
