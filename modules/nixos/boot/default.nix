{ ... }:
{
  boot.loader.efi = {
    canTouchEfiVariables = true;
    efiSysMountPoint = "/boot";
  };

  boot.loader.systemd-boot.enable = true;

  boot.initrd.systemd.enable = true;
}
