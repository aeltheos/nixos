{ inputs, pkgs, ... }:
(import inputs.treefmt-nix).mkWrapper pkgs {
  projectRootFile = "flake.nix";

  settings.excludes = [
    "*.pub"
    "*.lock"
  ];
  programs.nixfmt.enable = true;

  programs.mdformat.enable = true;

  programs.toml-sort.enable = true;
}
