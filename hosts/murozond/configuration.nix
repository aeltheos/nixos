{ flake, ... }:
{
  imports = [
    ./hardware-configuration.nix

    flake.nixosModules.core
    flake.nixosModules.boot
    flake.nixosModules.agenix
    flake.nixosModules.ceph-cluster
  ];

  system.stateVersion = "24.11";

  # Acquire network configuration from adh.crans.org via dhcp.
  networking.interfaces.eno1 = {
    useDHCP = true;
    macAddress = "14:02:ec:05:7b:7c";
  };

  networking.hostName = "murozond";
  networking.domain = "infra.aeltheos.eu";
  networking.hostId = "12345678";

  # we have lot of ram !
  boot.tmp.useTmpfs = true;

  # This is not correctly detected by nixos-generate --show-hardware-config
  boot.initrd.luks.devices.crypt-a = {
    preLVM = true;
    device = "/dev/disk/by-uuid/3c4821c2-0ced-4265-b38f-0e462dd28776";
    crypttabExtraOpts = [ "nofail" ];
  };
  boot.initrd.luks.devices.crypt-b = {
    preLVM = true;
    device = "/dev/disk/by-uuid/940a851e-124a-4f90-9137-50d01889531b";
    crypttabExtraOpts = [ "nofail" ];
  };

  # Configure ceph.
  services.ceph = {
    mon = {
      enable = true;
      daemons = [ "murozond" ];
    };
    osd = {
      enable = true;
      daemons = [
        "0"
        "1"
        "2"
        "3"
      ];
    };
    mgr = {
      enable = true;
      daemons = [ "murozond" ];
    };
    rgw = {
      enable = true;
      daemons = [ "murozond" ];
    };
  };
}
